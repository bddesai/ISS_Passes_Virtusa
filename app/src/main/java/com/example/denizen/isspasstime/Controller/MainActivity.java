package com.example.denizen.isspasstime.Controller;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.denizen.isspasstime.Model.Pass;
import com.example.denizen.isspasstime.Model.PassResponse;
import com.example.denizen.isspasstime.R;
import com.example.denizen.isspasstime.Service.ApiClient;
import com.example.denizen.isspasstime.Service.ApiInterface;
import com.example.denizen.isspasstime.Util;
import com.example.denizen.isspasstime.View.PassAdapter;

import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView = null;

    private ApiInterface apiService;

    private static String[] PERMISSIONS_GPS = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1;

    protected LocationManager locationManager;
    public String latitude, longitude;
    private AlertDialog progressDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUiComponents();

        checkPermissionsBeforeGettingCurrentLocation();

    }

    private void checkPermissionsBeforeGettingCurrentLocation() {
        //check for permissions and get current Location
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(PERMISSIONS_GPS, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            } else {
                // if the permissions are already granted
                getCurrentLocation();
            }

        } else {
            // you don't need the runtime permissions for older version
            getCurrentLocation();
        }
    }

    private void setupUiComponents() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new SpotsDialog(this);
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCurrentLocation();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    void getCurrentLocation() {
        try {
            progressDialog.show();
            // setup location manager
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        5000, 5, this);
            }
        } catch (SecurityException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        // get coordinates and process it
        Log.d(TAG, "Current Location: " + location.getLatitude() + ", " + location.getLongitude());
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
        downloadAndPopulateDate(latitude, longitude);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Util.showDialog("", getString(R.string.provider_disabled_text), this);
        progressDialog.dismiss();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        progressDialog.dismiss();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                   @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // when all the permissions are granted, proceed further
                getCurrentLocation();
            } else {
                Util.showDialog("", getString(R.string.permissions_not_granted), this);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void downloadAndPopulateDate(String latitude, String longitude) {

        if (latitude == null || longitude == null) {
            Log.d(TAG, String.format("Latitude=%s Longitude=%s", latitude, longitude));
            return;
        }

        // Get API Client
        apiService = ApiClient.getClient().create(ApiInterface.class);

        // Make an API Call
        Call<PassResponse> call = apiService.getPasses(latitude, longitude);

        // fetch and handle responses
        call.enqueue(new Callback<PassResponse>() {
            @Override
            public void onResponse(Call<PassResponse> call, Response<PassResponse> response) {


                List<Pass> passes = response.body().getPassArrayList();
                // populate passes
                recyclerView.setAdapter(new PassAdapter(passes,
                        R.layout.list_item_pass, MainActivity.this));

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<PassResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                Toast.makeText(MainActivity.this,
                        getString(R.string.service_error_message), Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
            }
        });
    }

}
