package com.example.denizen.isspasstime.Service;

import com.example.denizen.isspasstime.Model.PassResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public interface ApiInterface {
    @GET("iss-pass.json")
    Call<PassResponse> getPasses(@Query("lat") String latitude, @Query("lon") String longitude);
}
