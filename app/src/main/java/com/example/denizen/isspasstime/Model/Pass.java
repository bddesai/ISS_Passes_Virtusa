package com.example.denizen.isspasstime.Model;

import android.database.DatabaseUtils;

import com.example.denizen.isspasstime.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public class Pass {

    @SerializedName(Constants.DURATION)
    int duration;

    @SerializedName(Constants.RISETIME)
    long risetime;

    public Pass(int duration, long risetime) {
        this.duration = duration;
        this.risetime = risetime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public long getRisetime() {
        return risetime;
    }

    public void setRisetime(long risetime) {
        this.risetime = risetime;
    }
}
