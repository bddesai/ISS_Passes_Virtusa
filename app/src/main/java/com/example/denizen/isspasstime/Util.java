package com.example.denizen.isspasstime;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public class Util {

    public static Date getDateTimeFromTimeStamp(long timeStamp){
        Timestamp stamp = new Timestamp(timeStamp);
        Date date = new Date(stamp.getTime());
        return date;
    }


    public static void showDialog(String title, String message, Context context){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }
}
