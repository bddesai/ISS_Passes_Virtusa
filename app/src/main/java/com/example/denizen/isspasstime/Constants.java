package com.example.denizen.isspasstime;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public interface Constants {
    String BASE_URL = "http://api.open-notify.org/";

    String DURATION = "duration";
    String RISETIME = "risetime";
    String RESPONSE = "response";
    String DURATION_UNIT = " seconds";
}
