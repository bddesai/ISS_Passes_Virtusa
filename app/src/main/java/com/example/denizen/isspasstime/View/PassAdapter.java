package com.example.denizen.isspasstime.View;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.denizen.isspasstime.Constants;
import com.example.denizen.isspasstime.Util;
import com.example.denizen.isspasstime.Model.Pass;
import com.example.denizen.isspasstime.R;

import java.util.List;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public class PassAdapter extends RecyclerView.Adapter<PassAdapter.PassViewHolder> {

    List<Pass> passes;
    private int rowLayout;
    private Context context;

    public PassAdapter(List<Pass> passes, int rowLayout, Context context) {
        this.passes = passes;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public PassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new PassViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PassViewHolder holder, int position) {
        String duration = String.valueOf(passes.get(position).getDuration());
        String riseTime = String.valueOf(Util.getDateTimeFromTimeStamp(passes.get(position).getRisetime()));

        holder.duration.setText(duration + Constants.DURATION_UNIT);
        holder.riseTime.setText(riseTime);
    }

    @Override
    public int getItemCount() {
        return passes.size();
    }

    public static class PassViewHolder extends RecyclerView.ViewHolder {
        CardView passLayout;
        TextView duration;
        TextView riseTime;

        public PassViewHolder(View itemView) {
            super(itemView);
            passLayout = itemView.findViewById(R.id.passLayout);
            duration = itemView.findViewById(R.id.durationText);
            riseTime = itemView.findViewById(R.id.risetimeText);
        }
    }
}
