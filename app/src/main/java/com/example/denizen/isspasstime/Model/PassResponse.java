package com.example.denizen.isspasstime.Model;

import com.example.denizen.isspasstime.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public class PassResponse {
    public ArrayList<Pass> getPassArrayList() {
        return passArrayList;
    }

    public void setPassArrayList(ArrayList<Pass> passArrayList) {
        this.passArrayList = passArrayList;
    }

    @SerializedName(Constants.RESPONSE)
    private ArrayList<Pass> passArrayList;
}
